#  LiqPay SDK-PHP

Documentation https://www.liqpay.ua/documentation/en

## Install
to composer json:
```
"require": {
...
  "maxlen/liqpay": "dev-master",
},
"repositories": [
  ...
  {
    "type": "git",
    "url": "https://bitbucket.org/maxlen/liqpay.git"
  }
]
```
in console:
```php
composer require maxlen/liqpay
```